#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <stdbool.h>

#include "menu.h"

// Function that handles a single game event from the user
void playGame(int socket);

// Receives leaderboard from the Client and prints on screen
void printLeaderboard(int socket);

// Handles the list of letters that have previously been guessed.
void addLetter(char* list, char letter);

int main(int argc, char** argv){

  int sockfd; // Connection here
  int portNo;
  struct hostent *he;
  struct sockaddr_in their_addr; /* connector's address information */

  int menuChoice = 3;
 
  if(argc != 3){
    fprintf(stderr,"Usage: hangman <hostname> <Port #>\n");
		exit(1);
  }

  portNo = atoi(argv[2]);

  if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		herror("gethostbyname");
		exit(1);
	}

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
  }

  their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(portNo);    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr); // Get address of host
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */
  
  // Make connection
  if (connect(sockfd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
  }

  int available;
  int numbytes;
  if ((numbytes=recv(sockfd, &available, sizeof(int), MSG_WAITALL ) == -1)) {
    perror("recv");
    exit(1);
  }  
  if(available == -1){
    printf("Too many users are connected. Please try again later...\n");
    return 0;
  }
  
  if(logon(sockfd) == 0){
    printf("\n\nYou entered either an incorrect username or password - disconnecting.\n");
    close(sockfd);
    exit(0);
  }
  
  clearScr(); //Clear terminal
  menuChoice = menu();
  printf("Menuchoice : %d", menuChoice);
  fflush(stdout);
  

  while(menuChoice != 3){
    // Loop around waiting for menuChoice to = 3
    if ((numbytes = send(sockfd, &menuChoice, sizeof(int), 0)) == -1)
      perror("send");
  
    if(menuChoice == 1){
      playGame(sockfd);
      
    }
    if(menuChoice == 2){
      printLeaderboard(sockfd);
    }

    menuChoice = menu();
  }

  // User has chosen to quit, send this choice to Server
  if (send(sockfd, &menuChoice, sizeof(int), 0) == -1)
    perror("send");

  

  close(sockfd);

  return 0;

}

void playGame(int socket){
  
  printf("\e[1;1H\e[2J"); //Clear terminal
  char* phrase = calloc(30, sizeof(char));
  int guesses, length;
  char guess[10]; // Buffer can take more input, but only sends first
  int gameOver = 0;
  int numbytes;
  char letters[27] = {'\0'};
  letters[27] = '\0';

  // 1. Obtain number of guesses
  if (recv(socket, &guesses, sizeof(int), 0) == -1) 
    perror("recv");
  

  // 2. Obtain length of phrase
  if (recv(socket, &length, sizeof(int), 0) == -1)
    perror("recv");
  
  while(1){
    printf("\e[1;1H\e[2J"); //Clear terminal
    getchar();
    printf("\nGuessed letters: %s\n\n", letters); 

        
    printf("Number of guesses left: %d\n\n", guesses);
    // 3. Receive currently revealed phrase
    if ((numbytes = recv(socket, phrase, length, 0)) == -1) 
      perror("recv");
    
    printf("Word: %s\n\n", phrase);
    

    // 4. Receive gameOver status
    if (recv(socket, &gameOver, sizeof(int), 0) == -1) {
      perror("recv");
    } 

    if(gameOver != -1) // Check gameOver condition
      break;
    

    // Get guess
    printf("Enter your guess - ");
    fflush(stdout);
    fscanf(stdin,"%s", guess);
    while(guess[0] < 'a' || guess[0] > 'z'){
      printf("Please enter a letter from a - z:");
      fflush(stdout);
      fscanf(stdin,"%s", guess);
    }
    printf("Your guess is %c\n", *guess);
    addLetter(letters, guess[0]);
    
    // 5. Send guess to server
    if (send(socket, guess, sizeof(char), 0) == -1)
      perror("send");
    
    guesses = guesses - 1;
  }
  
  
  if(gameOver == 1) // Victory condition
    printf("\nWell done! You won this round of Hangman!\n\n");
  else
    printf("\nBad luck! You have run out of guesses. The Hangman got you!\n\n");

  free(phrase);

}

void printLeaderboard(int socket){
  clearScr();
  int users; // Number of users to display

  // 1. Receive number of users to be displayed
  if(recv(socket, &users, sizeof(int), 0) == -1)
    perror("send");

  if(users == 0){
    printf("%s", LEADERBOARD_DIVIDER);
    printf("No player data.\n\n");
    printf("%s", LEADERBOARD_DIVIDER);
    return;
  }
  
  char username[10];
  int wins;
  int played;
  for(int i = 0; i < users; i++){
    // 2. Get username
    if(recv(socket, username, 10, 0) == -1)
      perror("recv");
    // 3. Get Wins
    if(recv(socket, &wins, sizeof(int), 0) == -1)
      perror("recv");
    // 4. Get games played
    if(recv(socket, &played, sizeof(int), 0) == -1)
      perror("recv");

    printf("%s", LEADERBOARD_DIVIDER);
    printf("Player        - %s\n", username);
    printf("Games Won     - %d\n", wins);
    printf("Games Played  - %d\n\n", played);
    printf("%s", LEADERBOARD_DIVIDER);
  }
}

void addLetter(char* list, char letter){
  int i = 0;
  while(list[i] != '\0'){
    if(list[i] == letter) // if list contains letter already, just return
      return;
    i++;
  }
  // Assuming we made an end point, add to end of list.
  list[i] = letter;
}


