#include "menu.h"

const char WELCOME_MSG[] = "=============================================\n\n\n"
                          "Welcome to the Online Hangman Gaming System\n\n\n"
                          "=============================================\n\n\n\n";

const char DISPLAY_MENU[] = "Welcome to the Online Hangman Gaming System\n\n\n"
                              "Please enter a selection:\n"
                              "<1> Play Hangman\n"
                              "<2> Show Leaderboard\n"
                              "<3> Quit\n\n"
                              "Selection option 1-3-->";

const char LEADERBOARD_DIVIDER[] = "===================================\n\n";

void clearScr(){
  printf("\e[1;1H\e[2J");
}

bool logon(int socket){
  clearScr();

  // Variables to store username and password
  char username[10];
  char password[10];
    
  printf("%s", WELCOME_MSG);

  printf("Please enter your username-->");
  fscanf(stdin,"%s", username);
  printf("Please enter your password-->");
  fscanf(stdin, "%s", password);

  // Send username to server
  if (send(socket, username, 10, 0) == -1)
    perror("send");
  
  // Send password to server
  if (send(socket, password, 10, 0) == -1)
    perror("send");
  
  int pass;
  int numbytes;
  // Receive pass/fail from server
  if ((numbytes=recv(socket, &pass, sizeof(int), MSG_WAITALL ) == -1)){
    perror("recv");
    fflush(stdout);}

  printf("returning %d", pass);
  fflush(stdout);
 
  return pass;
}

int menu(){
  int selection;
  printf("%s", DISPLAY_MENU);

  scanf("%d", &selection);
  while((selection < 1) | (selection > 3)){
    printf("Enter an option between 1-3-->");
    scanf("%d", &selection);
  }
  return selection;
}
