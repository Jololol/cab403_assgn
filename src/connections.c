#include "connections.h"


void handleConnection(Client* client,leaderboard* board){
  // Status is used to signal the clients menu choices
  // 1 - User wishes to play a game
  // 2 - Users wishes to see the leaderboard
  // 3 - User wishes to quit.
  int status = 0; 
  int numbytes = 0;
  
  if(startConnection(client) == 0){
    printf("Failed logon attemp.\n");
    return;
  } 

  // Get unique userID for user.
  client->userId = addUser(board, client);
 
  while(status != 3){
        // Play a game
    if ((numbytes=recv(client->sockfd, &status, sizeof(int), 0 ) == -1)) {
      perror("recv");
    }  
        
    if(status == 1){
      printf("%s has started playing a game.\n", client->username);
      fflush(stdout);
      // Play a game, returning the win/lose score
      int score = gameHandle(client); 
      updateScore(board, client->userId, score); // Update score for client
    }
    // Selection 2 has been chosen, send the leaderboard to the client
    if(status == 2){
      printf("%s has requested to view the leaderboard\n", client->username);
      fflush(stdout);
      sendLeaderboard(client, board);
    }

  }

  printf("%s has logged off.\n", client->username);

}

// Receives username/password from Client and authenticates
int startConnection(Client* client){
  
  int numbytes;

  // Receiver username from client  
  if ((numbytes=recv(client->sockfd, client->username, 10, 0)) == -1) {
    perror("recv");
    exit(1);
  }
  // Receive password from client
  if ((numbytes=recv(client->sockfd, client->password, 10, 0)) == -1) {
    perror("recv");
    exit(1);
  }
  
  // Attempt authorisation
  int auth = authenticate(*client);

  // Send authorisation pass/fail back to client
  if (send(client->sockfd, &auth, sizeof(int), 0) == -1)
    perror("send");

  return auth;
  
}

// Pass in client for authentication
int authenticate(Client client){
  int pass = 0; // Assume failed unless proven otherwise
  char username[10];
  char password[10];

  FILE* file = fopen("Authentication.txt","r");
  char buffer[40];
  // Skip over first line of Authentication.txt
  fgets(buffer, 40, file);
  
  // Check supplied credentials against list
  while(!feof(file)){
    fgets(buffer, 40, file);
    sscanf(buffer, "%s %s", username, password);
    if((strcmp(client.username, username) == 0) 
         && (strcmp(client.password, password) == 0)) {
      pass = 1; // Authorisation successfull
      break;
    }
    
  }

  fclose(file);
  return pass;
}

  