#include "leaderboard.h"

void initLeaderboard(leaderboard* board){
  board->userNo = 0;
  // Initialise mutex objects
  pthread_mutex_init(&board->mutex, NULL);
  pthread_mutex_init(&board->rw_mutex, NULL);
  board->readCount = 0;
}

int addUser(leaderboard* board, Client* client){

  // Prevent two users being added simultaneously
  pthread_mutex_lock(&board->rw_mutex);
  int i;
  for( i = 0; i < board->userNo; i++){
    if((strcmp(board->users[i].username,client->username)) == 0){
      pthread_mutex_unlock(&board->rw_mutex);
      return i; // User already exists, keep their existing userId
    }
  }
  
  // Copy clients username into leaderboard
  strcpy(board->users[i].username, client->username);
  // Initialise stats to 0
  board->users[i].wins = 0;
  board->users[i].played = 0; 

  // Increase user counts
  board->userNo += 1;
  pthread_mutex_unlock(&board->rw_mutex);
  return i;
  
}

void updateScore(leaderboard* board, int userId, int score){
  // Lock the reader/writer mutex
  pthread_mutex_lock(&board->rw_mutex);
  board->users[userId].wins += score; // Update score
  board->users[userId].played++; // Update games played
  // Unlock the reader/writer mutex
  pthread_mutex_unlock(&board->rw_mutex);
}

void sendLeaderboard(Client* client, leaderboard* board){
  
  fflush(stdout);
  // Reader mutex process
  pthread_mutex_lock(&board->mutex);
  board->readCount++; // Incrememt read counter
  if(board->readCount == 1){
    pthread_mutex_lock(&board->rw_mutex);
  }
  pthread_mutex_unlock(&board->mutex);

  fflush(stdout);
  // Copy number of users into local variable
  int users = board->userNo; 
  // Fresh user list to be sorted, size of current number of users
  user* sortedList = (user *)malloc(sizeof(user) *users);

  //Perform deep copy of board data to sortedList
  for(int i = 0; i < users; i++){
    strcpy(sortedList[i].username,board->users[i].username);
    sortedList[i].wins = board->users[i].wins;
    sortedList[i].played = board->users[i].played;
  }

  //Perform in place sorting of sortedList
  for(int i = 0; i < users - 1;i++){
    int smallest = i; // Start with assumption i is smallest
    for(int j = i + 1; j < users; j++){
      
      if(sortedList[smallest].wins > sortedList[j].wins){
        smallest = j; // Swap if wins of next in list is smaller
      }
      else if(sortedList[smallest].wins == sortedList[j].wins){ // Check for equality
        float smallestRatio = sortedList[smallest].wins/sortedList[smallest].played; // Calculate i Ratio  
        float jRatio = sortedList[j].wins/sortedList[j].played;
        if(smallestRatio > jRatio){
          smallest = j; // Swap if jRatio is smaller then current smallest
        } else if (smallestRatio == jRatio){ // Final condition
          for(int k = 0; k < 10; k++){
            // Perform an alphabetical sort
            if(sortedList[smallest].username[k] == sortedList[j].username[k]) 
              continue; // Skip to next iteration if letters are equal
            if(sortedList[smallest].username[k] > sortedList[j].username[k]){
              break; // Smallest is valid, break out
            } else {
              smallest = j; // Smallest needs to be swapped
              break;
            }
          }
        }
      }
    }
    user temp; // Temp storage for doing swap
    // Do temp copy of i
    strcpy(temp.username,sortedList[i].username);
    temp.wins = sortedList[i].wins;
    temp.played = sortedList[i].played;
    // Swap places in list, i and smallest
    strcpy(sortedList[i].username,sortedList[smallest].username);
    sortedList[i].wins = sortedList[smallest].wins;
    sortedList[i].played = sortedList[smallest].played;

    // Copy temp back
    strcpy(sortedList[smallest].username,temp.username);
    sortedList[smallest].wins = temp.wins;
    sortedList[smallest].played = temp.played;

  }

  // Leaderboard reading section is finished
  // Resume reader mutex process
  pthread_mutex_lock(&board->mutex);
  board->readCount--;
  if(board->readCount == 0){
    pthread_mutex_unlock(&board->rw_mutex);
  }
  pthread_mutex_unlock(&board->mutex);

  // Calculate how many users stats to send
  int sendNo = 0;
  for(int i = 0; i < users; i++){
    if(sortedList[i].played > 0){
      sendNo++;
    }
  }

  // Start sending list to client.
  // 1. Let client know number of users arriving
  if (send(client->sockfd, &sendNo, sizeof(int), 0) == -1)
    perror("send");

  // Loop through userList sending each bit of data to client
  for(int i = (users - sendNo); i < users; i++){
    // 2. Send username
    if (send(client->sockfd,sortedList[i].username, 10, 0) == -1)
      perror("send");
    // 3. Send wins
    if(send(client->sockfd,&sortedList[i].wins, sizeof(int), 0) == -1)
      perror("send");
    // 4. Send played
    if(send(client->sockfd,&sortedList[i].played, sizeof(int), 0) == -1)
      perror("send");
  }
  

  // free sortedList allocation
  free(sortedList);
}

