// gameserver.h 
// Handles the logic of running a game for a Client.

#include "game.h"


#define PHRASE_NO 288


int gameHandle(Client* client){

  Phrase phrases[PHRASE_NO];       // Array to store all the phrase pairs

  // Load all the phrases in
  loadPhrases(phrases);
  
  int index; // Index of word pair to choose
  int gameOver = -1; // -1 means game still playing

  // Randomly pick pair from list.
  srand(time(NULL)); // Seed rand()
  index = rand() % PHRASE_NO; // Generate 0 - 287

  char currentGuess; // Stores the letter the client is currently guessing

  // Add two extra spaces, one for space, one for terminating character
  int length = strlen(phrases[index].type) + strlen(phrases[index].object) + 2; 
  int guesses = length + 8; // Determine number of guesses
  if(guesses > 26){
    guesses = 26;
  }

  // 1. Send number of guesses
  if (send(client->sockfd, &guesses, sizeof(int), 0) == -1)
    perror("send");

  // 2. Send length of phrase
  if (send(client->sockfd, &length, sizeof(int), 0) == -1)
    perror("send");

  // Phrase and revealed storage
  char* phrase = malloc(sizeof(char) * length);
  char* revealed = malloc(sizeof(char) * length);

  // Copy phrase into single char*
  strcpy(phrase,phrases[index].type);
  strcat(phrase," ");
  strcat(phrase,phrases[index].object);
  
  //Initialise revealed to underscores
  for(int i = 0; i < length - 1; i++){
    if(phrase[i] != ' '){
      revealed[i] = '_';
    } else {
      revealed[i] = ' ';
    }
  }
  phrase[length - 1] = '\0';   // Add termination character to end of string
  revealed[length - 1] = '\0'; // Add termination character to end of string

  // TODO Remove this printf statement before release
  printf("Chosen phrase %s\n", phrase);
  fflush(stdout);

  while(1){
    int numbytes;
    // 3. Send revealed phrase to client
    if ((numbytes = send(client->sockfd, revealed, length, 0)) == -1)
     perror("send");
   
    // 4. Send gameover Status to client
    if ((numbytes = send(client->sockfd, &gameOver, sizeof(int), 0)) == -1)
      perror("send");

    if(gameOver != -1) // gameOver condition
      break;

    // 5. Receive a guess from client
    if (recv(client->sockfd, &currentGuess, sizeof(char), 0) == -1) 
      perror("recv");
    
    // Process currentGuess against the phrase
    processGuess(currentGuess, phrase, revealed, length);
 
    // Check for gameOver condition
    if(strcmp(phrase, revealed) == 0)
      gameOver = 1; // Winning condition
    
    guesses = guesses - 1;
    if(guesses == 0)
      gameOver = 0; // Lose condition
  
  }

  // Free memory
  free(phrase);
  free(revealed);
  return gameOver;
}

void processGuess(char letter,char* phrase, char* revealed, int length){
  // Perform letter-by-letter guess
  for(int i = 0; i < length; i++){
    if(phrase[i] == letter){
      revealed[i] = letter;
    }
  }
}

void loadPhrases(Phrase* phrases){
  FILE* file = fopen("hangman_text.txt","r");
  
  char buffer[40];
  int i = 0; 
  while(!feof(file)){
    fgets(buffer, 40, file); // Load entire line into buffer
    char* token = strtok(buffer,","); // Get first string before ,
    strcpy(phrases[i].object, token); 
    token = strtok(NULL,"\n"); // Get last string before /n
    strcpy(phrases[i].type, token);   
    i++;
  }

}