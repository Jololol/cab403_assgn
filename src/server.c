#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <signal.h>

#include "clients.h"
#include "connections.h"

volatile sig_atomic_t exit_sig = 1;
int activeThreads[10] = {0}; // Keeps track of active threads
int active_sock_fd[10] = {0}; // Keeps a list of the active sock fds

#define CREDENTIAL_NO 10
#define PHRASE_NO 288

struct thread_request {
	int thread_id;
	struct Leaderboard* board;
	Client client;
};

// Checks for available threads.
int getThreadID();

// Handles a call to SIGINT
void exit_signal(int sig)
{
	exit_sig = 0;
	signal(sig, exit_signal);
}

void* startThread(void* arg)
{
	// Extract data from arg struct
	struct thread_request *request = (struct thread_request*)arg;
	Client newClient;
	newClient.sockfd = request->client.sockfd;
	int thread_id = request->thread_id;

	// Start the connection handling routine
	handleConnection(&newClient, request->board);

  activeThreads[thread_id] = 0; // Reset threadID
  close(active_sock_fd[thread_id]); // close connection
	return NULL;
}

int main(int argc, char** argv)
{

	int sockfd, new_fd;  /* listen on sock_fd, new connection on new_fd */
	struct sockaddr_in my_addr;    /* My Address */
	struct sockaddr_in their_addr; /* Connection address*/
	socklen_t sin_size;

	// File descriptor set for select
	fd_set readfds;

	pthread_t  p_threads[10];

	struct Leaderboard* board; // Struct to keep all of the board stats
	board = (leaderboard*)malloc(sizeof(leaderboard));
	initLeaderboard(board); // Initialise the leaderboard

	int portNo;
	if (argc != 2) {
		portNo = 12345;
	} else {
		portNo = atoi(argv[1]);
	}

	// Setting up SIGINT handler
	signal(SIGINT, exit_signal);

	// Get socket connection
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	/* generate the end point */
	my_addr.sin_family = AF_INET;         /* host byte order */
	my_addr.sin_port = htons(portNo);     /* short, network byte order */
	my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

	/* bind the socket to the end point */
	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		exit(1);
	}

	/* start listnening */
	if (listen(sockfd, 10) == -1) {
		perror("listen");
		exit(1);
	}

	printf("Server started listening on port %d\n", portNo);


	int numbytes;

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	while (exit_sig) {
    sin_size = sizeof(struct sockaddr_in);
    
    FD_ZERO(&readfds);
    FD_SET(sockfd, &readfds);
    FD_CLR(0, &readfds); // remove STDIN from set

    // Poll sockfd to see if an incoming connection
		select(sockfd + 1, &readfds, NULL, NULL, NULL);
		if (!(FD_ISSET(sockfd, &readfds))) {
			continue; // No signals, just loop back
		} else {
      // Check exit condition
      if(!exit_sig){
        break;
      }
      // We know a signal has arrived, so we accept
      if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
        perror("accept");
        continue;
      }
    }

		// Connection obtained, start a new thread request
		printf("server: got connection from %s\n", inet_ntoa(their_addr.sin_addr));
		struct thread_request request;
		request.client.sockfd = new_fd;
		request.board = board;
    int threadID;
    // Get threadID from threadpool
		if ((threadID = getThreadID()) == -1) {
			printf("No available threads...\n");
			if ((numbytes = send(new_fd, &threadID, sizeof(int), 0)) == -1)
				perror("send");
			close(new_fd);
		} else {

			if ((numbytes = send(new_fd, &threadID, sizeof(int), 0)) == -1)
				perror("send");
			request.thread_id = threadID;
			pthread_create(&p_threads[threadID], NULL, startThread, (void *)&request);
		}

		fflush(stdout);
	}

  // Handle shutdown.
  close(sockfd); // Close listening socket

  // Destroy all active threads
  for(int i = 0; i < 10; i++){
    if(activeThreads[i] == 1){
      close(active_sock_fd[i]); // Close socket
      pthread_kill(p_threads[i], SIGINT); // Destroy the thread
    }
  }

	printf("Shutting down server...\n");

	return 0;
}

int getThreadID()
{
	int threadID = -1; // -1 signals no threads available
	for (int i = 0; i < 10; i++) {
		if (activeThreads[i] == 0) {
			threadID = i;
			activeThreads[i] = 1; // Note this thread is active
			break;
		}
	}
	return threadID;
}


