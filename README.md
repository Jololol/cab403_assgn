# README #

### What is this repository for? ###

* My CAB403 assignment. 

### How do I get set up? ###

* Clone repository.
* Run make to build server and hangman client.
* Launch server by specifiying port number: ./server 12345
* Launch hangman client by specifying host and port number: ./hangman localhost 12345