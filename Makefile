
all: Client Server


Client: 
				gcc -Wall -pthread src/hangman.c src/menu.c -I./include  -o bin/hangman

Server: 
				gcc -Wall -pthread src/server.c src/game.c src/connections.c src/leaderboard.c -I./include -o bin/server


clean:
	rm bin/hangman bin/server
	rm -f *.o

.PHONY: all clean