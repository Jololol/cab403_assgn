// Connections.h and it's source file contains the methods that handle a
// the connection of a client to the server. 
#ifndef CONNECTIONS_H
#define CONNECTIONS_H

#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>

#include "leaderboard.h"
#include "game.h"
#include "clients.h"


// Handles a connection of a single client from start to finish
// Handles 
void handleConnection(Client* client, leaderboard* board);

// Starts connetion by receiving username/password from client program
// and performs authentication test.
int startConnection(Client* client);

// Authenticates a new clients credentials against
// Authentication.txt
int authenticate(Client client);

#endif