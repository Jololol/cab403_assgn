// gameserver.h 
// Contains the functions which handle the running of a game for a client.

#ifndef GAME_H
#define GAME_H

#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>

#include "clients.h"



typedef struct Phrases{
  char object[20];
  char type[20];
}Phrase;

// Initialises a game by choosing a phrase. 
// Handles receiving guesses from the client, as well as processing
// those guesses, and sending the Client the revealed portion of the word.
int gameHandle(Client* client);

// Process a supplied guess against the phrase and reveals letter(s)
// if guess is correct.
void processGuess(char letter,char* phrase, char* revealed, int length);

// Load phrases into an array for random selection
void loadPhrases(Phrase* phrases);

#endif