// Leaderboard.h defines a Leaderboard struct which containsa ll the information for the leaderboard
// Such as a list of users (containing their wins, and games played) as well as mutex 
// objects to protect critical sections.

#ifndef LEADERBOARD_H
#define LEADERBBOARD_H


#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <pthread.h>
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>

#include "clients.h"

// Contains data about a specific user such as games won and played
typedef struct User{
  char username[10];
  int wins;
  int played;
}user;

// A struct to contain all the information for a leaderboard, as well as
// mutex objects to prevent two clients writing simultaneously.
typedef struct Leaderboard{
  user users[10]; // A list to store currently registered users.
  int userNo; // Keep track of how many users have actually played
  pthread_mutex_t mutex; // A mutex used to protect updating leaderboard
  pthread_mutex_t rw_mutex; // A reader-writer mutex to allow multiple read access, but deny more then 1 writer
  int readCount; // Keeps track of number of users currently reading.
}leaderboard;

// Initialise and setup the leaderboard to use
void initLeaderboard(leaderboard* board);

// Add a client to the user list. Returns a unique userId for a new client
// if client has previously connected, they receive the same userId
int addUser(leaderboard* board, Client* client);

// Updates the clients score.
void updateScore(leaderboard* board, int userId, int score);

// Sends the leaderboard through to a client upon request.
void sendLeaderboard(Client* client, leaderboard* board);


#endif