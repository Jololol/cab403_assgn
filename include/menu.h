#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <stdbool.h>

// Contains strings used to print UI to screen
extern const char WELCOME_MSG[];
extern const char DISPLAY_MENU[];
extern const char LEADERBOARD_DIVIDER[];

// Clears screen for neater look.
void clearScr();

// Draws UI and gets username and password from user 
// Requires a socket connection to pass username/password 
// back to client for authentication
bool logon(int socket);

// Draws main menu and returns user selection as an int
int menu();



