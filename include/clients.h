// Header file contains a simple struct called Client
// This struct is used to contain the information for a particular client
// including it's socket id, username, password and unique userID
#ifndef CLIENTS_H
#define CLIENTS_H


typedef struct Clients{
  int sockfd; // Clients unique socket fd
  int userId; // Unique userId used to reference user across multiple logins
  char username[10];
  char password[10];
} Client;

#endif
